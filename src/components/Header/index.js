import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './style.css'

export default class Header extends Component {
  render() {
    return (
        <div className='header'>
          <h1 className='title'>Anotações</h1>
            <div className='right'>
              <Link to="/">Notas</Link>
              <Link to="/about">Sobre</Link>
            </div>
        </div>
    )
  }
}
